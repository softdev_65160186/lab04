/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab04;

/**
 *
 * @author PC
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player player1;
    private Player player2;
    private Player currentPlayer;
    private int turnCount = 0;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public char[][] getTable() {
        return table;
    }

    public boolean setRowCol(int row, int col) {
        if (row < 1 || row > 3 || col < 1 || col > 3 || table[row - 1][col - 1] != '-') {
            return false;
        }
        table[row - 1][col - 1] = currentPlayer.getSymbol();
        turnCount++;
        return true;
    }

    public boolean checkWin() {
        return checkRow() || checkCol() || checkDiagonal();
    }

    private boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == currentPlayer.getSymbol() &&
                table[i][1] == currentPlayer.getSymbol() &&
                table[i][2] == currentPlayer.getSymbol()) {
                return true;
            }
        }
        return false;
    }

    private boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[0][i] == currentPlayer.getSymbol() &&
                table[1][i] == currentPlayer.getSymbol() &&
                table[2][i] == currentPlayer.getSymbol()) {
                return true;
            }
        }
        return false;
    }

    private boolean checkDiagonal() {
        if (table[0][0] == currentPlayer.getSymbol() &&
            table[1][1] == currentPlayer.getSymbol() &&
            table[2][2] == currentPlayer.getSymbol()) {
            return true;
        }
        if (table[0][2] == currentPlayer.getSymbol() &&
            table[1][1] == currentPlayer.getSymbol() &&
            table[2][0] == currentPlayer.getSymbol()) {
            return true;
        }
        return false;
    }

    public boolean checkDraw() {
        return turnCount == 9;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }
}

