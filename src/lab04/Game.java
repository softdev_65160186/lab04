/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab04;

import java.util.Scanner;

/**
 *
 * @author PC
 */

public class Game {

    private Table table;
    private Player player1, player2;

    public Game() {
        player1 = new Player('O');
        player2 = new Player('X');
    }

    public void play() {
        showWelcome();
        while (true) {
            newGame();
            showTable();
            playGame();
            checkContinue();
        }
    }

    private void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j]);
            }
            System.out.println();
        }
    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void playGame() {
        while (true) {
            showTurn();
            inputRowCol();
            showTable();
            if (table.checkWin()) {
                System.out.println(table.getCurrentPlayer().getSymbol() + " wins!");
                table.getCurrentPlayer().win();
                break;
            } else if (table.checkDraw()) {
                System.out.println("It's a draw!");
                player1.draw();
                player2.draw();
                break;
            }
            table.switchPlayer();
        }
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row col: ");
            int row = sc.nextInt();
            int col = sc.nextInt();
            if (table.setRowCol(row, col)) {
                break;
            } else {
                System.out.println("Invalid move. Try again.");
            }
        }
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " turn");
    }

    private void checkContinue() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Continue playing? (y/n): ");
            String answer = sc.next();
            if (answer.equals("n")) {
                System.out.println("Thanks for playing!");
                System.exit(0);
            } else if (answer.equals("y")) {
                break;
            } else {
                System.out.println("Invalid input. Please enter 'y' or 'n'.");
            }
        }
    }

    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }
}

